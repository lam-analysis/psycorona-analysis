---
title: 3N Model Summary
format:
  html: default
  pdf: default
knitr:
  opts_chunk:
    comment: "|"
    message: false
    out.width: "\\textwidth"
    fig.height: 6
    fig.width: 10
execute:
  cache: true
  echo: true
  eval: true
  error: false
  warning: false
bibliography: ref.bib
---

```{r init, echo = FALSE}
#| cache: false

pkgs <- c("magrittr", "lavaan", "gtsummary", "semPlot")
pkgs.load <- sapply(pkgs, library, character.only = TRUE)

options(digits = 2)
tbl3n <- readr::read_csv("../data/raw/data-3n.csv")
mod3n <- readRDS("../RData/model-3n-baseline.Rds")

tbl_score    <- readr::read_csv("../data/processed/scored_data.csv")
mod3n_merged <- readRDS("../RData/model-3n-merged.Rds")

```

# Methods

## Source of Data {#sec-data}

The current investigation used baseline data as sourced from the PsyCorona project, specifically the following variables:

- **Perceived financial strain**  
  Five-choice Likert-like items to measure agreement on financial-related issues [@selenko2011beyond].
  - `PFS01`: I am financially strained
  - `PFS02`: I often think about my current financial situation
  - `PFS03`: Due to my financial situation, I have difficulties paying for my expenses
- **Loneliness**  
  Five-choice items evaluating perceived loneliness during the past week [@seligman2011flourish].
  - `lone01`: Did you feel lonely?
  - `lone02`: Did you feel isolated from others?
  - `lone03`: Did you feel left out?
- **Happiness**  
  A single-item Likert scale scoring 1-10 to measure happiness, as devised by @abdel2006measuring, the only question was "In general, how happy would you say you are?" (`happy`). In this scale, higher numbers represent higher happiness level.
- **Disempowerment**  
  Five-choice Likert-like items to measure the agreement on the state of being disempowered [@Leander2019].
  - `fail01`: Not a lot is done for people like me in this country
  - `fail02`: If I compare people like me against other people in this country, my group is worse off
- **COVID-19 community response**  
  Five-choice Likert-like items to measure community response towards COVID-19 [@leander2011mind]. The measurement is represented by the action of a part or the whole of a community.
  - `c19NormDo`: Right now, people in my area do self-isolate and engage in social distancing
  - `c19IsStrict`: To what extent is your community developing strict rules in response to the Coronavirus?
- **Radical collective action**  
  Seven-choice Likert-like items to measure collective actions amidst COVID-19 pandemic which have a stronger inclination towards radical behaviors [@van2008toward]. All items were measured as the tendency of signing a petition to support particular actions.
  - `c19RCA01`: Supports mandatory vaccination once a vaccine has been developed for Coronavirus
  - `c19RCA02`: Supports mandatory quarantine for those that have Coronavirus and those that have been exposed to the virus
  - `c19RCA01`: Supports  reporting people who are suspected to have Coronavirus

## 3N Models

The needs-narrative-network (3N) model was initially specified disempowerment to reflect collective-significant needs; while perceived financial strain, loneliness, and happiness to reflect the personal-significant needs. Then, networks in this model were indicated by COVID-19 community response. Finally, the narrative was represented as radical collective action. Our model evaluate the path where both collective and personal needs influenced the associated networks in determining radical actions.

```{mermaid}
%%| out.width: auto
%%| echo: false
%%| fig-cap: "A conceptual framework of the 3N model"
%%| label: fig-mermaid

flowchart LR
  per.sig[Personal needs] --> norm[Community Response]
  col.sig[Collective needs] --> norm
  norm --> rca[Radical Collective Action]

```

```{r}
#| echo: false
#| eval: false

form <- "
  # Latent model
  collective =~ fail01 + fail02
  personal   =~ PFS01 + PFS02 + PFS03 + lone01 + lone02 + lone03 + happy
  norm       =~ c19NormDo + c19IsStrict
  RCA        =~ c19RCA01 + c19RCA02 + c19RCA03

  # Correlation
  norm ~ collective + personal
  RCA  ~  norm

  # Controlling for error
  lone01 ~~ lone02 + lone03
  lone02 ~~ lone03
"

```


## Statistical Analysis

To empirically evaluate the 3N model hypothesis, we employed a structural equation model per @fig-mermaid. Each component in the conceptual framework was represented as a latent variable, calculated using manifest as described in @sec-data. The goodness of fit indicators were SRMR < 0.1, RMSEA < 0.08, CFI > 0.9, and TLI 0.9; when the model did not satisfy the goodness of fit criteria, remodelling was done based on theoretical remark and the largest modification index.

# Results

```{r}
#| echo: false

getLine <- function(mod, line_term) {
  subset(mod %>% broom::tidy(), term == line_term)
}

report <- function(modline, ...) {
  ci <- round(modline$estimate + 1.96 * c(-1, 1) * modline$std.error, 2)
  sprintf(
    "%s [%s - %s]",
    modline$estimate %>% round(2),
    ci[[1]],
    ci[[2]]
  )
}

col_sig <- getLine(mod3n_merged, "norm ~ collective")
per_sig <- getLine(mod3n_merged, "norm ~ personal")
rca     <- getLine(mod3n_merged, "RCA ~ norm")

```

As suggested by @fig-3n, both collective- and personal-significant needs covary with one to another. Collective-significant needs, as reflected by disempowerment, negatively contributed to COVID-19 community response with a standardized effect estimate of $\beta$ = `r report(col_sig)`. On the other hand, the personal-significant needs positively contributed to COVID-19 community response with $\beta$ = `r report(per_sig)`. These findings implied that perception of being disempowered discouraged the society to conform to an amiable community response. Though presented with a positive slope, the magnitude of personal needs was three times weaker compared to the collective needs, suggesting that collective needs were the dominant factor in determining community responses. In turn, community responses then determined the radical collective action, where $\beta$ = `r report(rca)`, demonstrating that an amiable community response resulted in a better compliance to the health policy.

## Descriptive statistical summary

```{r, echo = FALSE}
#| tbl-cap: "Descriptive summary of the dataset, grouped by gender"

tbl_score %>%
  select(PFS01, PFS02, PFS03, lone01:happy, fail01, fail02, c19NormDo, c19IsStrict, collective:RCA, gender) %>%
  dplyr::mutate(
    gender = dplyr::case_when(gender == 1 ~ "Female", gender == 2 ~ "Male", gender == 3 ~ "Other")
  ) %>%
  tbl_summary(by = gender, type = list(everything() ~ "continuous")) %>%
  add_p(everything() ~ "aov") %>%
  as_kable_extra(booktabs = TRUE, longtable = TRUE) %>%
  kableExtra::kable_styling(latex_options = "repeat_header")

```

## 3N model goodness of fit

```{r}

fitMeasures(
  mod3n_merged,
  fit.measures = c("rmsea", "rmsea.ci.lower", "rmsea.ci.upper", "srmr", "cfi", "tli"),
  fm.args      = list("rmsea.ci.level" = 0.95)
) %>%
  data.frame()

```

## 3N model summary

```{r}

broom::tidy(mod3n_merged) %>%
  subset(select = -op) %>%
  knitr::kable(booktabs = TRUE, longtable = TRUE, digits = 2) %>%
  kableExtra::kable_styling(latex_options = "repeat_header")

```

## Visualizing the 3N model

```{r, echo = FALSE}
#| label: fig-3n
#| fig-cap: Needs-narrative-network model of radical collective actions amidst the COVID-19 pandemic

semPaths(mod3n_merged, whatLabels = "est", style = "lisrel", nCharNodes = 0)

```

# References

