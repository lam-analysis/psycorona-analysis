# Analysis on the PsyCorona dataset

PsyCorona is an ambitious research project commenced during COVID-19 pandemic
which provide an extensive longitudinal data from 115 countries. This analysis
focuses on modelling the latent growth curve of various observed variables. To
run the analysis scripts provided in the `src/` directory, you will need to
request the `data.csv` file from our collaborator then stashed it as
`data/raw/data.csv`. The scripts in `src/*.R` will subsequently process the
`data.csv` file and later return processed dataset in both wide format, long
format, and aggregated wide format. We also provided an imputation method using
MICE (multiple imputation by chained equations), which require ~4 hours runtime
duration.
