Task 2023-05-24:

- Add statistical analysis under the methods section
- Explain why we didn't conduct CFA and directly did SEM instead
- Interpret the latent variable model
- Calculate the true score, then create subject profile based on age, sex, and country

Task 2023-02-06:

- The network var predicts RCA (C-19 extremism)
- Disempowerment should discard fail03
- Personal vars should discard PFS
- Variables:
  - Collective sig: fail0{1..2}
  - Personal sig: PFS0{1..3}, lone0{1..3}, happy
  - Descriptive norm: c19NormDo, c19IsStrict $\to$ Vars is not currently available in the complete dataset
  - COVID-19 RCA: c19RCA0{1..3}
- Model:
  - Descriptive norm ~ Collective sig + Personal sig
  - RCA ~ Descriptive norm

Task 2023-01-31:

- Create a mixed-effect regression:
  - DV: COVID-19 radical collective action
  - IV: Societal discontent, perceived financial strain, disempowerment, loneliness, life satisfaction, self esteem
  - RV: Country level
- Path analysis to prove the construct of a 3N model:
  - DV: COVID-19 radical collective action
  - Collective: Societal discontent, disempowerment
  - Personal: Perceived financial strain, loneliness, life satisfaction, self esteem
  - Mediator: Trust to government and/or community
- Generate a multi-level path analysis using the same specification as above

Task 2023-01-26:

- Use long.tbl data, not the imputed table
- Sourcing the country based on c19RCA0[1-3] $\to$ Take the top 10
- Also include the following countries:
  - Indonesia
  - Saudi
  - USA
  - UK
  - Germany
  - Australia
  - Japan
- Use emotion as an independent variable $\to$ Include disc01:lifeSat
- Use perceived threats as a moderator:
  - Trust to government
  - Conspiracy
  - Migrant threat
- Do hierarchical regression $\to$ Perform LMER using week as a RV
- POC $\to$ Does C-19 wave influence RCA? $\to$ Analyze on countries above, use JHU

Task 2023-01-20:

- Evaluate the exponential growth from the LGC model
- Explore the delta between week
- Sinusoidal function to model the changes overtime
- Use either relweek/epiweek $\to$ Take only the first 5/6 weeks to avoid
  sparsity

Task 2022-11-08:

- Do either:
  - PCA
  - Latent variable analysis
- The step above is required to group all vars into Red, Green, and Blue var groups
- After performing the step above, proceed with latent growth curve analysis

===

Hypothesis:
- Can we predict the difference between decline and growth?
- What variance at individual vs national level?

Note:
- Harmonization technique previously used: https://docs.google.com/document/d/1DuuWvdCahyyt4CBkwGz5LkBMj0jEo6Cv6ov3V8FyU8w
- Null data $\to$ List-wise deletion, or EM imputation, or bootstrapping
- Country-level variable $\to$ In term of COVID-19 policy and mortality $\to$ Using Oxford database

Data:
- PsyCorona
- Oxford C19 tracker:
  - https://covidtracker.bsg.ox.ac.uk/
  - https://github.com/OxCGRT/covid-policy-tracker/blob/master/documentation/codebook.md

Log:

- Do dataviz: --> URGENT!
  - Create a dot plot, each dot represents the mean (aggregate on the country level)
  - Use c19RCA as the Y axis
  - Use trustGovCtry, trustGovState, lone, consp as the X axis
  - Highlight the dot representing Indonesia
- Red is the DV of interest
