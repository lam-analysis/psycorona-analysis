#PUTI Q! Data Analysis - Latent Profile Analysis USA Sample#

##install the package
library(tidyLPA)
library(dplyr)
library(haven)

##import the data
dat.usa <- read_sav("country sample/usa.sav")
View(dat.usa)

##comparing the solutions
#---2-class---#
LPA.usa2x <- dat.usa%>%
  select(collective, personal, norm, RCA)%>%
  single_imputation()%>%
  scale()%>%
  estimate_profiles(2, variances = "equal", covariances = "zero")
LPA.usa2x
plot_profiles(LPA.usa2x, add_line = T)

get_data(LPA.usa2x) %>% print(n=Inf)
get_fit(LPA.usa2x) %>% print(n=Inf)
get_estimates(LPA.usa2x) %>% print(n=Inf)

usa.res2x <- get_data(LPA.usa2x) %>% print(n=Inf)
usa.fit2x <- get_fit(LPA.usa2x) %>% print(n=Inf)
usa.est2x <- get_estimates(LPA.usa2x) %>% print(n=Inf)

#View full output and export as excel sheet
View(usa.res2x)
write.csv(usa.res2x, file="puti usa 2 class-res.csv")
View(usa.fit2x)
write.csv(usa.fit2x, file="puti usa 2 class-fit.csv")
View(usa.est2x)
write.csv(usa.est2x, file="puti usa 2 class-est.csv")

#---3-class---#
LPA.usa3x <- dat.usa%>%
  select(collective, personal, norm, RCA)%>%
  single_imputation()%>%
  scale()%>%
  estimate_profiles(3, variances = "equal", covariances = "zero")
LPA.usa3x
plot_profiles(LPA.usa3x, add_line = T)

get_data(LPA.usa3x) %>% print(n=Inf)
get_fit(LPA.usa3x) %>% print(n=Inf)
get_estimates(LPA.usa3x) %>% print(n=Inf)

usa.res3x <- get_data(LPA.usa3x) %>% print(n=Inf)
usa.fit3x <- get_fit(LPA.usa3x) %>% print(n=Inf)
usa.est3x <- get_estimates(LPA.usa3x) %>% print(n=Inf)

#View full output and export as excel sheet
View(usa.res3x)
write.csv(usa.res3x, file="puti usa 3 class-res.csv")
View(usa.fit3x)
write.csv(usa.fit3x, file="puti usa 3 class-fit.csv")
View(usa.est3x)
write.csv(usa.est3x, file="puti usa 3 class-est.csv")

#---4-class---#
LPA.usa4x <- dat.usa%>%
  select(collective, personal, norm, RCA)%>%
  single_imputation()%>%
  scale()%>%
  estimate_profiles(4, variances = "equal", covariances = "zero")
LPA.usa4x
plot_profiles(LPA.usa4x, add_line = T)

get_data(LPA.usa4x) %>% print(n=Inf)
get_fit(LPA.usa4x) %>% print(n=Inf)
get_estimates(LPA.usa4x) %>% print(n=Inf)

usa.res4x <- get_data(LPA.usa4x) %>% print(n=Inf)
usa.fit4x <- get_fit(LPA.usa4x) %>% print(n=Inf)
usa.est4x <- get_estimates(LPA.usa4x) %>% print(n=Inf)

#View full output and export as excel sheet
View(usa.res4x)
write.csv(usa.res4x, file="puti usa 4 class-res.csv")
View(usa.fit4x)
write.csv(usa.fit4x, file="puti usa 4 class-fit.csv")
View(usa.est4x)
write.csv(usa.est4x, file="puti usa 4 class-est.csv")

#---5-class---#
LPA.usa5x <- dat.usa%>%
  select(collective, personal, norm, RCA)%>%
  single_imputation()%>%
  scale()%>%
  estimate_profiles(5, variances = "equal", covariances = "zero")
LPA.usa5x
plot_profiles(LPA.usa5x, add_line = T)

get_data(LPA.usa5x) %>% print(n=Inf)
get_fit(LPA.usa5x) %>% print(n=Inf)
get_estimates(LPA.usa5x) %>% print(n=Inf)

usa.res5x <- get_data(LPA.usa5x) %>% print(n=Inf)
usa.fit5x <- get_fit(LPA.usa5x) %>% print(n=Inf)
usa.est5x <- get_estimates(LPA.usa5x) %>% print(n=Inf)

#View full output and export as excel sheet
View(usa.res5x)
write.csv(usa.res5x, file="puti usa 5 class-res.csv")
View(usa.fit5x)
write.csv(usa.fit5x, file="puti usa 5 class-fit.csv")
View(usa.est5x)
write.csv(usa.est5x, file="puti usa 5 class-est.csv")