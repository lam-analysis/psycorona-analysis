## LOAD PACKAGES

pkgs <- c("magrittr")
pkgs.load <- sapply(pkgs, library, character.only = TRUE)


## PREP

# Load dataset
imp.tbls <- list(
  "sub"  = readr::read_csv("data/processed/imp-sub.csv"),
  "wide" = readr::read_csv("data/processed/imp-wide.csv")
)

# Load function and regex
load("RData/00-subset-data.RData")

# Set the grouping variables
groups <- list(
  "green" = c(
    "disc", "PFS", "fail", "lone", "happy", "lifeSat", "MLQ", "_SE",
    "impactCtrl", "impactBelong", "vacOthMeth", "trustGovtCtry",
    "trustGovState", "trustBusiness"
  ),
  "red"   = c(
    "c19RCA", "ecoRCA", "covVaccSafe", "covVacEff", "covVacHarm", "covVacMicro",
    "covVacDNA", "genVacHarm", "genVacMicro", "migrantThreat", "feelingTherm",
    "consp"
  ),
  "blue"  = c("CAident", "CAprotest")
) %>%
  lapply(function(group) {
    tbl     <- imp.tbls$sub # Set the table to subset from
    weeks   <- paste0("w", 1:18) %>% set_names(., .)
    lapply(weeks, function(week) {
      # Iterate variable name on weekly basis
      week %<>% {sprintf("^%s_.*", .)}
      regex[group] %>% sapply(function(reg) {
        # Select the variable names based on regular expression
        reg     <- gsub(x = reg, "\\^\\.\\*", week)
        varname <- find.name(tbl, reg, perl = TRUE)
        tryCatch(
          subset(tbl, select = varname),
          error = function(e) NA
        )
      }) %>%
        {do.call(cbind, .)} %>%
        data.frame() %>%
        tibble::tibble() %>%
        {.[, colSums(is.na(.)) < nrow(.)]} %>%
        set_colnames(gsub(x = names(.), ".*_", "")) # Clean up the colnames
    }) %>%
    extract(which(sapply(., ncol) > 0)) # Extract non-empty tibbles
  })

# Per group per week correlation matrix for sanity check
lapply(groups, function(weeks) {
  lapply(weeks, cor)
})


## PCA

# Extract the first principal component per week per group
tbl.pca <- lapply(groups, function(group) {
  lapply(names(group), function(weekname) {
    week <- group[[weekname]]
    prcomp(week, scale. = TRUE, rank. = 1) %>%
      predict() %>%
      data.frame() %>%
      set_names(weekname)
  }) %>%
    {do.call(cbind, .)}
}) %>%
  {do.call(cbind, .)}


## WRITE

readr::write_csv(tbl.pca, file = "data/processed/tbl-pca.csv")
