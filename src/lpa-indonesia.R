#PUTI Q! Data Analysis - Latent Profile Analysis ind Sample#

##install the package
library(tidyLPA)
library(dplyr)
library(haven)

##import the data
dat.ind <- read_sav("country sample/indonesia.sav")
View(dat.ind)

##comparing the solutions
#---2-class---#
LPA.ind2x <- dat.ind%>%
  select(collective, personal, norm, RCA)%>%
  single_imputation()%>%
  scale()%>%
  estimate_profiles(2, variances = "equal", covariances = "zero")
LPA.ind2x
plot_profiles(LPA.ind2x, add_line = T)

get_data(LPA.ind2x) %>% print(n=Inf)
get_fit(LPA.ind2x) %>% print(n=Inf)
get_estimates(LPA.ind2x) %>% print(n=Inf)

ind.res2x <- get_data(LPA.ind2x) %>% print(n=Inf)
ind.fit2x <- get_fit(LPA.ind2x) %>% print(n=Inf)
ind.est2x <- get_estimates(LPA.ind2x) %>% print(n=Inf)

#View full output and export as excel sheet
View(ind.res2x)
write.csv(ind.res2x, file="puti ind 2 class-res.csv")
View(ind.fit2x)
write.csv(ind.fit2x, file="puti ind 2 class-fit.csv")
View(ind.est2x)
write.csv(ind.est2x, file="puti ind 2 class-est.csv")

#---3-class---#
LPA.ind3x <- dat.ind%>%
  select(collective, personal, norm, RCA)%>%
  single_imputation()%>%
  scale()%>%
  estimate_profiles(3, variances = "equal", covariances = "zero")
LPA.ind3x
plot_profiles(LPA.ind3x, add_line = T)

get_data(LPA.ind3x) %>% print(n=Inf)
get_fit(LPA.ind3x) %>% print(n=Inf)
get_estimates(LPA.ind3x) %>% print(n=Inf)

ind.res3x <- get_data(LPA.ind3x) %>% print(n=Inf)
ind.fit3x <- get_fit(LPA.ind3x) %>% print(n=Inf)
ind.est3x <- get_estimates(LPA.ind3x) %>% print(n=Inf)

#View full output and export as excel sheet
View(ind.res3x)
write.csv(ind.res3x, file="puti ind 3 class-res.csv")
View(ind.fit3x)
write.csv(ind.fit3x, file="puti ind 3 class-fit.csv")
View(ind.est3x)
write.csv(ind.est3x, file="puti ind 3 class-est.csv")

#---4-class---#
LPA.ind4x <- dat.ind%>%
  select(collective, personal, norm, RCA)%>%
  single_imputation()%>%
  scale()%>%
  estimate_profiles(4, variances = "equal", covariances = "zero")
LPA.ind4x
plot_profiles(LPA.ind4x, add_line = T)

get_data(LPA.ind4x) %>% print(n=Inf)
get_fit(LPA.ind4x) %>% print(n=Inf)
get_estimates(LPA.ind4x) %>% print(n=Inf)

ind.res4x <- get_data(LPA.ind4x) %>% print(n=Inf)
ind.fit4x <- get_fit(LPA.ind4x) %>% print(n=Inf)
ind.est4x <- get_estimates(LPA.ind4x) %>% print(n=Inf)

#View full output and export as excel sheet
View(ind.res4x)
write.csv(ind.res4x, file="puti ind 4 class-res.csv")
View(ind.fit4x)
write.csv(ind.fit4x, file="puti ind 4 class-fit.csv")
View(ind.est4x)
write.csv(ind.est4x, file="puti ind 4 class-est.csv")

#---5-class---#
LPA.ind5x <- dat.ind%>%
  select(collective, personal, norm, RCA)%>%
  single_imputation()%>%
  scale()%>%
  estimate_profiles(5, variances = "equal", covariances = "zero")
LPA.ind5x
plot_profiles(LPA.ind5x, add_line = T)

get_data(LPA.ind5x) %>% print(n=Inf)
get_fit(LPA.ind5x) %>% print(n=Inf)
get_estimates(LPA.ind5x) %>% print(n=Inf)

ind.res5x <- get_data(LPA.ind5x) %>% print(n=Inf)
ind.fit5x <- get_fit(LPA.ind5x) %>% print(n=Inf)
ind.est5x <- get_estimates(LPA.ind5x) %>% print(n=Inf)

#View full output and export as excel sheet
View(ind.res5x)
write.csv(ind.res5x, file="puti ind 5 class-res.csv")
View(ind.fit5x)
write.csv(ind.fit5x, file="puti ind 5 class-fit.csv")
View(ind.est5x)
write.csv(ind.est5x, file="puti ind 5 class-est.csv")