## LOAD PACKAGES

pkgs <- c("magrittr", "lavaan")
pkgs.load <- sapply(pkgs, library, character.only = TRUE)


## PREP

# Read the first principal data frame
tbl.pca <- readr::read_csv("data/processed/tbl-pca.csv")

# Load the `mkForm` function
load("RData/04-latent-growth.RData")
load("RData/00-subset-data.RData")

# Create the list of fomulas
mod.form <- c("green", "red", "blue") %>%
  set_names(., .) %>%
  lapply(function(group) {
    find.name(tbl.pca, group) %>%
      mkForm(pattern = "\\..*\\d")
  })


## STAT

# Fit the latent growth curve model
models <- lapply(mod.form, function(mod) {
  growth(mod, data = tbl.pca)
})

lapply(models, summary, fit.measure = TRUE)

# Controlling the covariance according to modification index
var.ctrl <- "
  red.w8  ~~ red.w10
  red.w17 ~~ red.w18
  red.w6  ~~ red.w8
  red.w6  ~~ red.w10
"

# Confounders and determinants
var.iv <- "
  s.red   ~  s.green + s.blue
"

# Latent growth of all models
paste(mod.form, collapse = "\n") %>%
  paste(var.ctrl, sep = "\n") %>%
  growth(data = tbl.pca) %T>%
  {print(fitmeasures(.))} %>%
  #{print(summary(., fit.measure = TRUE))} %>%
  modindices(sort = T) %>%
  head()

